** # NanoPC **
---

* HuyLe `anhhuy@live.com` To developer C code, bash shell phython scripts on system NanoPC-T3. Also mantain bootloader(uboot), kernel driver(fedora os).
* Alice `mahongthuy@gmail.com` To design java scripts, css, html language

---

** 1. Change hostname at the files **

> * /etc/hostname 
> * /etc/hosts

** 2. Update the package to OS **

> * Set default editor: `sudo update-alternatives --config editor`

```
apt-get install update
apt-get install ssh git screen cscope udhcpc ethtool meld vim iperf
sudo apt-get install -y isc-dhcp-server xinetd tftpd-hpa wget apache2
```

** 3. Streaming the video **

```
apt-get install make libjpeg-dev libv4l-dev
```
> * Refer at site: [LINK](https://www.acmesystems.it/video_streaming)

** 4. Set default INTERFACE **

```
/etc/default/isc-dhcp-server INTERFACE="wlan0"
```

** 5. Setup git **

> * Configure user git
```
git config --global user.email "anhhuy@live.com"
git config --global user.name "HuyLe"
update-alternatives --config editor
```

> * Add git lg show all commited
```
git config --global alias.lg "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
```
> * Git Tags
```
git tag -a v1.x $commited -m "example: release new version"
git push --tags
git show v1.x
```
** 6. How to setup NAT and FORWARD? [LINK](https://gist.github.com/kimus/9315140) ** 

> * Need install ufw package
```
apt-get install ufw
```
> * NAT FORWARD setup
>> - In the file `/etc/default/ufw` change the parameter DEFAULT_FORWARD_POLICY
```
DEFAULT_FORWARD_POLICY="ACCEPT"
```
>> - Also configure `/etc/ufw/sysctl.conf` to allow ipv4/ipv6 forwarding.
```
net.ipv4.ip_forward=1
net/ipv6/conf/default/forwarding=1
net/ipv6/conf/all/forwarding=1
```
>> - The with ubuntu OS add the following to `/etc/ufw/before`.rules just before the filter rules.
```
# NAT table rules
*nat
:POSTROUTING ACCEPT [0:0]

# Forward traffic through eth0 - Change to match you out-interface
-A POSTROUTING -s 10.27.10.0/24 -o eth0 -j MASQUERADE

# don't delete the 'COMMIT' line or these nat table rules won't
# be processed
COMMIT
```
>> - The Fedora OS add the following /etc/rc.local file.
```
#iptables -t nat -A POSTROUTING -s 192.168.8.0/24 ! -d 192.168.8.0/24  -j MASQUERADE
iptables -t nat -A POSTROUTING -s 10.27.10.0/24 -o eth0  -j MASQUERADE

```
>> - Now enable the changes by restarting ufw.
```
sudo ufw disable && sudo ufw enable
```
---
** # NETWORKING **

> * Iwlist command Get more detailed wireless information from a wireless interface. A typical usage is as follows:
```
Usage: iwlist [interface] scanning [essid NNN] [last]
              [interface] frequency 
              [interface] channel 
              [interface] bitrate 
              [interface] rate 
              [interface] encryption 
              [interface] keys 
              [interface] power 
              [interface] txpower 
              [interface] retry 
              [interface] ap 
              [interface] accesspoints 
              [interface] peers 
              [interface] event 
              [interface] auth 
              [interface] wpakeys 
              [interface] genie 
              [interface] modulation 
```

---
** # THE TOOLS **

* VNCServer: [LNIK](http://vnraspberrypi.blogspot.com/2016/01/huong-dan-cai-at-he-ieu-hanh-va-mot-so.html)

---
** # THE ISSUE **

> 1. ISSUE 0001
```
perl: warning: Setting locale failed.
perl: warning: Please check that your locale settings:
	LANGUAGE = (unset),
	LC_ALL = (unset),
	LC_PAPER = "vi_VN",
	LC_ADDRESS = "vi_VN",
	LC_MONETARY = "vi_VN",
	LC_NUMERIC = "vi_VN",
	LC_TELEPHONE = "vi_VN",
	LC_IDENTIFICATION = "vi_VN",
	LC_MEASUREMENT = "vi_VN",
	LC_TIME = "vi_VN",
	LC_NAME = "vi_VN",
	LANG = "en_US.UTF-8"
    are supported and installed on your system.
perl: warning: Falling back to a fallback locale ("en_US.UTF-8").
```
> * Fixed
```
export LANGUAGE=en_US.UTF-8
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_TYPE=en_US.UTF-8
```

---
### Owner: HuyLe 
* Email: [anhhuy@live.com](email)
* skype: [sdv_huyle](skype)