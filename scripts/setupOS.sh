#!/bin/bash
echo "To setup vim bashrc screen to OS"
cp -rf .vimrc .screenrc .bashrc ~/
echo "Configuring NAT FORWARDING"
cp -rf ufw /etc/default/
cp -rf before.rules /etc/ufw/
cp -rf sysctl.conf /etc/ufw/
echo "Updateing server dhcpd usb0 to wlan0"
cp -rf isc-dhcp-server /etc/default/
echo "Updating rc.local for NAT & Ad-Hoc networking"
cp -rf wpa_supplicant.conf /etc/wpa_supplicant/
cp -rf rc.local /etc/
echo "Configure dhcpd"
cp -rf dhcpd.conf /etc/dhcp/
echo "FINISHED"
